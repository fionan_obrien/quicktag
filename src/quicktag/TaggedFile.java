/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package quicktag;

import java.io.File;
import java.util.ArrayList;

/**
 *
 * @author fiona
 */
public class TaggedFile extends File {

    ArrayList<Tag> tags = new ArrayList();

    public TaggedFile(String pathname) {
        super(pathname);
    }

    boolean add(Tag t) {
        return tags.add(t);
    }

    @Override
    public String toString() {

        StringBuilder sb = new StringBuilder(super.toString());

        tags.stream().forEach((t) -> {
            sb.append(t);
        });

        return super.toString();
    }

}
