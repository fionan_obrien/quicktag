/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package quicktag;

import java.util.ArrayList;

/**
 *
 * @author fiona
 */
public class Tag {

    public static ArrayList<Tag> tags = new ArrayList();
    Tag parent = null;
    String name;

    public Tag(String s) {
        this.name = s;
        tags.add(this);
    }

    public Tag(String s, Tag t) {
        this.name = s;
        this.parent = t;
        tags.add(this);

    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("");
        if(parent!=null){
            
            sb.append(super.toString()+",");
        }
        
        return sb.append(name).toString();
    }
    
    

}
