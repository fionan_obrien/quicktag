/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package quicktag;

import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.LayoutManager;
import java.util.List;
import java.awt.Toolkit;
import java.awt.datatransfer.DataFlavor;
import java.awt.dnd.DnDConstants;
import java.awt.dnd.DropTarget;
import java.awt.dnd.DropTargetDropEvent;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import javax.imageio.ImageIO;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JEditorPane;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.filechooser.FileSystemView;

/**
 *
 * @author fiona
 */
public class Viewer extends JFrame {

    static final Dimension d = new Dimension(500, 400);
    static final Color[] COLORS = {Color.yellow, Color.PINK, Color.CYAN};
    static JTextArea myPanel;
    final Database database;

    static Image logo;

    public Viewer(Database database) {
        super("QuickTag");
        this.database = database;
        Container c = this.getContentPane();
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setSize(d);
        c.setLayout(new GridBagLayout());
        logo = Toolkit.getDefaultToolkit().getImage(getClass().getResource("/res/QuickTagLogo.png"));
        this.setIconImage(logo);

        c.add(getDropPanel(d));
        c.add(getTagPanel(d, database));

        this.pack();
        this.setVisible(true);

    }

    JPanel getDropPanel(Dimension d) {
        JPanel jp = new JPanel();
        Dimension size = new Dimension(d.width / 2, d.height);
        jp.setPreferredSize(size);

        jp.setBackground(COLORS[1]);
        ImageIcon ii = new ImageIcon("/res/QuickTagLogo.png");
        JLabel label = new JLabel("DropTarget");
        label.setIcon(ii);

        jp.add(label);
        jp.add(showEditorPane());
        jp.repaint();
        jp.setVisible(true);
        return jp;
    }

    JPanel getTagPanel(Dimension d, Database data) {
        JPanel jp = new JPanel();
        Dimension size = new Dimension(d.width / 2, d.height);

        jp.setPreferredSize(size);

        jp.setBackground(COLORS[0]);

        showDatabase(jp, data);

        jp.setVisible(true);

        return jp;

    }

    JTextArea showEditorPane() {
        JEditorPane jedit = new JEditorPane();

        myPanel = new JTextArea();
        myPanel.setPreferredSize(d);
        myPanel.setBackground(COLORS[2]);

        myPanel.setDropTarget(new DropTarget() {
            @Override
            public synchronized void drop(DropTargetDropEvent evt) {
                try {
                    evt.acceptDrop(DnDConstants.ACTION_COPY);
                    List<File> droppedFiles = (List<File>) evt.getTransferable().getTransferData(DataFlavor.javaFileListFlavor);

                    for (File file : droppedFiles) {

                        System.out.println("Help i recived " + file.getAbsolutePath());
                        myPanel.add(getThumbnail(file));
                        myPanel.repaint();
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        });

        return myPanel;

    }

    public void showDatabase(JPanel panel, Database database) {

        System.out.println("");
        for (Tag t : database.softCopy) {

            panel.add(new JButton(t.name));
            System.out.println(t.name);
        }

    }

    JLabel getThumbnail(File f) {
         Icon icon = FileSystemView.getFileSystemView().getSystemIcon(f);
         Image img = ((ImageIcon) icon).getImage().getScaledInstance(100,100,Image.SCALE_SMOOTH);
         ImageIcon icon1 = new ImageIcon(img);
         
        return new JLabel(icon1);

    }

}
