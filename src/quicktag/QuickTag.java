/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package quicktag;

import java.io.File;
import java.io.IOException;

/**
 *
 * @author fiona
 */
public class QuickTag {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException {

        Database data = new Database(new File("database.txt"));

        data.load();

        System.out.println(data.softCopy.size());

        Viewer view = new Viewer(data);

    }

}
