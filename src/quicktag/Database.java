/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package quicktag;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Scanner;

/**
 *
 * @author fiona
 */
public class Database {

    File f;

    HashSet<Tag> softCopy = new HashSet<>();
    HashSet<TaggedFile> taggedFiles = new HashSet();

    public Database(File f) {
        this.f = f;

    }

    boolean add(Tag t) {

        return true;

    }

    boolean update() throws IOException {

        FileWriter fw = new FileWriter(f, true);

        for (Tag t : softCopy) {

            fw.write(t.toString() + "\n");
            fw.flush();
            fw.close();
        }

        return false;
    }

    boolean load() throws FileNotFoundException, IOException {

        Scanner sc = new Scanner(f);

        while (sc.hasNext()) {

            String s = sc.nextLine();

            String[] parse = s.split(":");

            TaggedFile file = new TaggedFile(parse[0]);

            for (int i = 1; i < parse.length; i++) {
                file.add(new Tag(parse[i]));
                softCopy.add(new Tag(parse[i]));
            }
            taggedFiles.add(file);

        }

        return true;
    }

    Tag parseTag(String s) {
        Tag tag = null;

        String[] parse = s.split(":");

        return tag;
    }

    Tag[] parseForTags(String s) {

        String[] asString = s.split(":");

        Tag[] tags = new Tag[asString.length - 1];
        int i = 0;
        for (String str : asString) {
            tags[i++] = new Tag(str);

        }
        return tags;
    }

}
